<?php

/*
    Plugin Name: Ставки 
    Description: Создаёт произвольный тип постов «Ставки».
    Version: 1.0
    Author: Дмитрий Ядыкин
*/

/**
 * yl_rates - класс для работы со ставками
 * 
 * @version 1.0
 */
class yl_rates 
{
    const rates_code = 'yl_rates';
    
    public function __construct() 
    {
        //Создаёт новый тип постов
        add_action( 'init', array( $this, 'create_post_type' ) );
        //Создаёт новые таксономии
        add_action( 'init', array( $this, 'create_taxonomy' ) );
    }
    
    /**
     * Создаёт новый тип ставки в WP
     */
    public function create_post_type()
    {
        register_post_type( self::rates_code,
            array(
                'labels' => array(
                  'name' => __( 'Ставки' ),
                  'singular_name' => __( 'Ставку' ),
                ),
                'public' => true,
                'has_archive' => true,
                'rewrite' =>  array('slug' => 'rates'),
                'menu_position' =>  5,
                'menu_icon' =>  'dashicons-chart-area',
            )
          );
    }
    
    /**
     * Создатёт новые таксономии
     */
    public function create_taxonomy ()
    {
        $arTaxonomy = $this->get_taxonomy();
        
        foreach ($arTaxonomy as $arItem)
        {
            register_taxonomy(
                self::rates_code . '_' . $arItem['slug'],
                self::rates_code,
                array(
                    'label' => __( $arItem['label'] ),
                    'rewrite' => array( 'slug' => $arItem['slug'] ),
                    'hierarchical' => true,
                    'labels' => array (
                        'edit_item' => __( 'Редактировать' ),
                        'add_new_item' => __( 'Добавить' ),
                    ),
                )
            );
        }
    }
    
    /**
     * Описывает таксономию
     * 
     * @return Array 
     */
    public function get_taxonomy ()
    {
        return array(
            array(
                'label' => 'Тип ставки',
                'slug' => 'type'
            ),
            array(
                'label' => 'Статус ставки',
                'slug' => 'status'
            )
        );
    }
    
}

/**
 * yl_roles - класс для работы с ролями
 * 
 * @version 1.0
 */
class yl_roles
{
    const roles_code = 'yl_roles';
    
    public function __construct() 
    {
        //Создаёт роли при активации
        register_activation_hook( __FILE__, array( $this, 'add_roles' ) );
        //Удаляет роли при деактивации
        register_deactivation_hook( __FILE__, array( $this, 'remove_roles' ) );
    }
    
    /**
     * Добавляет роль
     */
    public function add_roles() 
    {
        $arRoles = $this->get_roles();
        
        foreach ($arRoles as $role)
        {
            add_role( self::roles_code . '_' . $role['role'], $role['display_name'], $role['capabilities'] );
        }
    }
    
    /**
     * Удаляет роль
     */
    public function remove_roles() 
    {
        $arRoles = $this->get_roles();
        
        foreach ($arRoles as $role)
        {
            remove_role( self::roles_code . '_' . $role['role'] );
        }
    }
    
    /**
     * Описывает роли
     * @return Array
     */
    public function get_roles ()
    {
        return array(
            array(
                'role' => 'capper', 
                'display_name' => 'Каппер',
                'capabilities' => array( 
                    'edit_posts' => true, 
                    'delete_posts' => true, 
                    'read' => true, 
                )
            ),
            array(
                'role' => 'moderator', 
                'display_name' => 'Модератор',
                'capabilities' => array( 
                    'edit_published_posts' => true, 
                    'publish_posts' => true, 
                    'delete_published_posts' => true, 
                    'edit_posts' => true, 
                    'delete_posts' => true, 
                    'read' => true, 
                )
            ),
        );
    }
}

$yl_rates = new yl_rates();

$yl_roles = new yl_roles(); 